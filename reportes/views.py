# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView
from django.core.urlresolvers import reverse_lazy
from io import BytesIO
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table
from reportlab.lib.units import cm
from reportlab.lib import colors
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet
from django.http import HttpResponse
from reportlab.graphics.shapes import Image, Drawing
from denuncia.forms import DenunciaForm
from denuncia.models import Denuncia
from victima.forms import VictimaForm
from victima.models import Victima
from agresor.forms import AgresorForm


# Create your views here
class ReportList(ListView):
    model = Denuncia
    template_name = 'crud/reportes/reportes_listar.html'

class ReportRango(CreateView):
    template_name = 'crud/reportes/rangof_form.html'
    success_url = reverse_lazy('reportes:reportes_listar')
    model = Denuncia
    form_class = DenunciaForm
    second_form_class = VictimaForm

    def get_context_data(self, **kwargs):
        context = super(ReportRango, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return context


def generar_pdf_rango(request):

    finicial = request.POST['fechadenuncia']
    ffinal = request.POST['fechanacimiento']
    indice = 0
    response = HttpResponse(content_type='application/pdf')
    pdf_name = "RangoDeFechas.pdf"  # llamado clientes
    # la linea 26 es por si deseas descargar el pdf a tu computadora
    # response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    clientes = []

    logo = 'static/img/lolto.jpg'
    im = Image(180, -40, 180, 90, logo)
    styles = getSampleStyleSheet()
    dibujo = Drawing(30, 30)
    dibujo.add(im)
    clientes.append(dibujo)
    clientes.append(Spacer(1,35))
    header = Paragraph("Reporte por Rango de Fechas", styles['Heading1'])
    header2 = Paragraph("PGN", styles['Heading2'])
    header3 = Paragraph(finicial + " a " + ffinal, styles['Heading3'])
    clientes.append(header)
    clientes.append(header2)
    clientes.append(header3)
    if Denuncia.objects.filter(fechadenuncia__range=(finicial, ffinal)).count() == 0:
        clientes.append(Spacer(1, 25))
        line = Paragraph("NO EXISTEN DENUNCIAS EN ESTE RANGO DE FECHAS",
                         styles['Heading4'])
        clientes.append(line)
        clientes.append(Spacer(1, 40))
        doc.build(clientes)
        response.write(buff.getvalue())
        buff.close()
        return response

    else:

        headings = ('Correlativo', 'Victima', 'Agresor',  'Estado', 'Carpeta Judicial','Fecha')
        allclientes = [(denun.correlativo, denun.victima.Nombre_Completo(), denun.Agresor.Nombre_Completo(),
                   denun.status, denun.carpeta, denun.fechadenuncia ) for denun in Denuncia.objects.filter(fechadenuncia__range=(finicial, ffinal))]
        t = Table([headings] + allclientes, colWidths=[2 * cm, 5 * cm, 5 * cm, 2 * cm , 3 * cm, 2.2 * cm])
        t.setStyle(TableStyle(
         [
            # La primera fila(encabezados) va a estar centrada
            ('ALIGN', (0, 0), (3, 0), 'CENTER'),
            # Los bordes de todas las celdas serán de color negro y con un grosor de 1
            ('GRID', (0, 0), (-1, -1), 0.4, colors.black),
            # El tamaño de las letras de cada una de las celdas será de 10
            ('FONTSIZE', (0, 0), (-1, -1), 9),
         ]
            ))
        clientes.append(Spacer(1, 25))
        clientes.append(t)
        line5 = Paragraph("¡Y conoceréis la verdad, y la verdad os hará libres.!",
                          styles['Heading3'])
        line6 = Paragraph("¡Juan 8:32. | RVR1960 |.!", styles['Heading4'])
        clientes.append(line5)
        clientes.append(line6)
        clientes.append(Spacer(1, 25))
        clientes.append(Spacer(1, 40))
        clientes.append(Spacer(0, 15))
        doc.build(clientes)
        response.write(buff.getvalue())
        buff.close()
        return response

def generar_ficha_pdfd(request ,pk):

    response = HttpResponse(content_type='application/pdf')
    pdf_name = "FichaDenunciaPdf.pdf"  # llamado clientes
    # la linea 26 es por si deseas descargar el pdf a tu computadora
    # response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    clientes = []

    logo = 'static/img/lolto.jpg'
    im = Image(180, -40, 180, 90, logo)
    styles = getSampleStyleSheet()
    dibujo = Drawing(30, 30)
    dibujo.add(im)
    clientes.append(dibujo)
    clientes.append(Spacer(1,35))
    header = Paragraph("FICHA DE DENUNCIA", styles['Heading1'])
    clientes.append(Spacer(1, 35))
    header2 = Paragraph("Procuradoria General de la Nación", styles['Heading2'])
    header3= Paragraph("Esta ficha hace constar al portador el registro de la denuncia con los siguientes datos: ",
                       styles['Heading3'])
    clientes.append(header)
    clientes.append(header2)
    clientes.append(header3)
    headings = ('Correlativo',
                'Carpeta Judicial',
                'Victima',
                'Agresor',
                'Fecha de denuncia')
    allclientes = [(total.correlativo, total.carpeta, total.victima.Nombre_Completo(),total.Agresor.Nombre_Completo(),
                    total.fechadenuncia) for total
                   in Denuncia.objects.all().filter(id=pk)]
    t = Table([headings] + allclientes, colWidths=[1.9 * cm, 2.8 * cm, 5 * cm, 5* cm, 3.5 * cm])
    t.setStyle(TableStyle(
        [
            # La primera fila(encabezados) va a estar centrada
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            # Los bordes de todas las celdas serán de color negro y con un grosor de 1
            ('GRID', (0, 0), (-1, -1), 0.4, colors.black),
            # El tamaño de las letras de cada una de las celdas será de 10
            ('FONTSIZE', (0, 0), (-1, -1), 10),
            ('BACKGROUND', (0, 0), (-1, 0), colors.pink),
        ]
    ))

    clientes.append(t)
    clientes.append(Spacer(0, 15))
    doc.build(clientes)
    response.write(buff.getvalue())
    buff.close()
    return response

# Create your views here.
