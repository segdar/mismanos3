from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from reportes.views import ReportList, ReportRango, generar_pdf_rango, generar_ficha_pdfd



# from django.conf import settings
# from django.conf.urls.static import static

# admin.autodiscover()

urlpatterns = [
                  url(r'^listar', login_required(ReportList.as_view()), name='reportes_listar'),
                  url(r'^rango_f', login_required(ReportRango.as_view()), name='rango'),
                  url(r'^ficha/(?P<pk>\d+)/$', login_required(generar_ficha_pdfd), name='ficha'),
                  url(r'^rangopdf/', login_required(generar_pdf_rango), name='pdf_rango'),

              ]
