# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from derechos.models import Derecho

admin.site.register(Derecho)
# Register your models here.
