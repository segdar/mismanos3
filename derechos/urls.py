from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from derechos.views import DerechoList, DerechoCreate, \
    DerechoUpdate, DerechoDelete, DerechoDenunciaCreate

# from django.conf import settings
# from django.conf.urls.static import static

# admin.autodiscover()

urlpatterns = [
                  url(r'^nuevo$', login_required(DerechoCreate.as_view()), name='derecho_crear'),
                  url(r'^listar/$', login_required(DerechoList.as_view()), name='derecho_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(DerechoUpdate.as_view()), name='derecho_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(DerechoDelete.as_view()), name='derecho_eliminar'),
                  url(r'^nuevo_para_denuncia/$', login_required(DerechoDenunciaCreate.as_view()), name='dediladd'),
              ]
