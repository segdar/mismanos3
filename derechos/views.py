# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator


from derechos.forms import DerechoForm, Derecho_DiligenciaForm
from derechos.models import Derecho


# Create your views here.

class DerechoList(ListView):
    model = Derecho
    template_name = 'crud/Derechos/derechos_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(DerechoList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(derecho__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('derechos.add_derechos', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DerechoList, self).dispatch(*args, **kwargs)



class DerechoCreate(CreateView):
    model = Derecho
    form_class = DerechoForm
    template_name = 'crud/Derechos/derechos_form.html'
    success_url = reverse_lazy('derechos:derecho_listar')


    @method_decorator(
        permission_required('derechos.add_derechos', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DerechoCreate, self).dispatch(*args, **kwargs)


class DerechoDenunciaCreate(CreateView):
    model = Derecho
    form_class = Derecho_DiligenciaForm
    template_name = 'crud/Derechos/derechos_form_denuncia.html'
    success_url = reverse_lazy('denuncia:denuncia_crear')

"""
by: Eddav3 follow me on twitter
"""


class DerechoUpdate(UpdateView):
    model = Derecho
    form_class = DerechoForm
    template_name = 'crud/Derechos/derechos_form.html'
    success_url = reverse_lazy('derechos:derecho_listar')


class DerechoDelete(DeleteView):
    model = Derecho
    template_name = 'crud/Derechos/derechos_eliminar.html'
    success_url = reverse_lazy('derechos:derecho_listar')

# Create your views here.
