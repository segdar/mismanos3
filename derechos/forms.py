# -*- coding: utf-8 -*-
from django import forms
from derechos.models import Derecho

class DerechoForm(forms.ModelForm):
    class Meta:
        model = Derecho

        fields = [
            'derecho',
        ]

        labels = {
            'nombre': 'Derecho:',

        }

        widgets = {
            'derecho': forms.TextInput(attrs={'class': 'form-control'}),
        }


class Derecho_DiligenciaForm(forms.ModelForm):
    class Meta:
        model = Derecho

        fields = [
            'derecho',
        ]

        labels = {
            'nombre': 'Derecho:',
        }

        widgets = {
            'derecho': forms.TextInput(attrs={'class': 'form-control'}),
        }
