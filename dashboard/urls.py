from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from dashboard.views import inicio


urlpatterns = [
    url(r'^principal', inicio, name="dashboard_inicio"),

]
