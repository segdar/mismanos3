"""sid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
    By Eddav3 Follow me On Twitter
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth.views import login, logout_then_login
from django.conf import settings

urlpatterns = [

    url(r'^$', login, {'template_name':'index.html'}, name='login'),
    url(r'^accounts/login/', login, {'template_name': 'index.html'}, name='login'),
    url(r'^logout/', logout_then_login, name='logout'),
    url(r'^dashboard/', include('dashboard.urls', namespace="dashboard")),
    url(r'^tipo_diligencia/', include('tipo_diligencia.urls', namespace="diligencia")),
    url(r'^victima/', include('victima.urls', namespace="victima")),
    url(r'^agresor/', include('agresor.urls', namespace="agresor")),
    url(r'^procedencia_denuncia/', include('procedencia_denuncia.urls', namespace="procedencia")),
    url(r'^derechos/', include('derechos.urls', namespace="derechos")),
    url(r'^denuncia/', include('denuncia.urls', namespace="denuncia")),
    url(r'^reportes/', include('reportes.urls', namespace="reportes")),
    url(r'^admin/', admin.site.urls)

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT)

