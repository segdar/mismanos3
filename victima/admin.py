# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from victima.models import Victima

admin.site.register(Victima)

# Register your models here.
