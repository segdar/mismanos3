# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

from victima.forms import VictimaForm, VictimaDenuciaForm
from victima.models import Victima


# Create your views here
class VictimaList(ListView):
    model = Victima
    template_name = 'crud/victima/victima_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(VictimaList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(nombres__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(apellidos__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(cui__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(telefono__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(direccion__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('victima.add_victima', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(VictimaList, self).dispatch(*args, **kwargs)


class VictimaCrear(CreateView):
    model = Victima
    form_class = VictimaForm
    template_name = 'crud/victima/victima_form.html'
    success_url = reverse_lazy('victima:victima_listar')

    @method_decorator(
        permission_required('victima.add_victima', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(VictimaCrear, self).dispatch(*args, **kwargs)


class VictimaUpdate(UpdateView):
    model = Victima
    form_class = VictimaForm
    template_name = 'crud/victima/victima_form.html'
    success_url = reverse_lazy('victima:victima_listar')


class VictimaDelete(DeleteView):
    model = Victima
    template_name = 'crud/victima/victima_eliminar.html'
    success_url = reverse_lazy('victima:victima_listar')

class VictimaDenunciaCreate(CreateView):
    model = Victima
    form_class = VictimaDenuciaForm
    template_name = 'crud/victima/victima_form_denuncia.html'
    success_url = reverse_lazy('denuncia:denuncia_crear')
