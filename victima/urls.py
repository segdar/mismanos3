from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from victima.views import VictimaList, \
    VictimaUpdate, VictimaDelete, VictimaCrear, VictimaDenunciaCreate


urlpatterns = [
                  url(r'^nuevo', login_required(VictimaCrear.as_view()), name='victima_crear'),
                  url(r'^listar', login_required(VictimaList.as_view()), name='victima_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(VictimaUpdate.as_view()), name='victima_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(VictimaDelete.as_view()), name='victima_eliminar'),
                  url(r'^CrearDenuncia/$', login_required(VictimaDenunciaCreate.as_view()), name='dediladd'),
              ]
