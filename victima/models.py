# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from persona.models import Persona


# Create your models here.


class Victima(Persona):

 fotografia = models.ImageField(db_column='fotografia',
                                upload_to='images', null=True, blank=True, help_text="Carge una Fotografia de la Victima.")

 def image_(self):
     return '<a href="/media/{0}"><img src="/media/{0}" height="100" width="100"></a>'.format(self.fotografia)
 image_.allow_tags = True

 def Nombre_Completo(self):
     return self.nombres + ' ' + self.apellidos
     Nombre_Completo.short_description = "Nombre Completo"
     n_completo = property(Nombre_Completo)

 def __unicode__(self):
     return '%s %s' % (self.nombres, self.apellidos)


 class Meta:

    verbose_name = 'victima'
    verbose_name_plural = 'Victimas'