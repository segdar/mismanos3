# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from denuncia.models import Denuncia, Correlativo
from django.contrib import admin


class DenunciaAdmin(admin.ModelAdmin):

 list_display = ['correlativo','fechadenuncia', 'diligencia', 'procedencia', 'victima', 'Agresor',  'victima', 'status',
                 'carpeta', 'description']
# Register your models here.
class CorrelativoAdmin(admin.ModelAdmin):
    if Denuncia.objects.all().count() >= 1:
        def get_actions(self, request):
            actions = super(CorrelativoAdmin, self).get_actions(request)  # Obtenemos
            del actions['delete_selected']
        def has_delete_permission(self, request, obj=None):
            return False
        def has_add_permission(self, request, obj=None):
            return False

admin.site.register(Correlativo, CorrelativoAdmin)
admin.site.register(Denuncia, DenunciaAdmin)
