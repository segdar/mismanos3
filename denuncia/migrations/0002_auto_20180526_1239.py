# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-26 12:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('denuncia', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='correlativo',
            name='correlative',
            field=models.PositiveIntegerField(help_text='Ingrese el Correlativo Inicial', verbose_name='Correlativo:'),
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='victima',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='victima.Victima', verbose_name='Victima'),
        ),
    ]
