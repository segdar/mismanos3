# -*- coding: utf-8 -*-
from django import forms
from denuncia.models import Denuncia

class DateInput(forms.DateInput):
    input_type = 'date'
    format = ('d-m-Y')


class DenunciaForm(forms.ModelForm):
    class Meta:
        model = Denuncia

        fields = [
            'fechadenuncia',
            'victima',
            'Agresor',
            'derecho',
            'status',
            'diligencia',
            'procedencia',
            'carpeta',
            'description',
        ]

        labels = {
            'fechadenuncia': 'Fecha de Denuncia:',
            'victima': 'Victima:',
            'Agresor': 'Agresor:',
            'derecho': 'Derecho Vulnerado:',
            'status': 'Estado de la Denuncia:',
            'diligencia': 'Diligencia Correspondiente:',
            'procedencia':'Tipo de Procedencia',
            'carpeta': 'No. De Carpeta Judicial:',
            'description': 'Ingrese una Descripción de la Denuncia:',
        }

        widgets = {
            'fechadenuncia':   DateInput(attrs={'class': 'form-control'}),
            'victima': forms.Select(attrs={'class': 'form-control'}),
            'Agresor': forms.Select(attrs={'class': 'form-control'}),
            'derecho': forms.Select(attrs={'class': 'form-control'}),
            'status': forms.CheckboxInput(attrs={'class': 'form-control'}),
            'diligencia': forms.Select(attrs={'class': 'form-control'}),
            'procedencia': forms.Select(attrs={'class': 'form-control'}),
            'carpeta': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }
