# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from tipo_diligencia.models import Diligencia
from procedencia_denuncia.models import Procedencia
from victima.models import Victima
from agresor.models import Agresor
from derechos.models import Derecho

# Create your models here.

SELECCION_TIPO = (

    ('I', 'Inicial'),
)
class Correlativo(models.Model):

 correlative = models.IntegerField('Correlativo:', help_text='Ingrese el Correlativo Inicial')

 def __unicode__(self):
  return '%s' % self.correlative

 class META:
    verbose_name = 'Correlativo'
    verbose_plural_name = 'Correlativos'


class Denuncia(models.Model):

 fechadenuncia= models.DateField('Fecha de Denuncia', help_text='Ingrese Fecha en que se realiza la denuncia')
 diligencia = models.ForeignKey(Diligencia, verbose_name='Diligencia')
 procedencia = models.ForeignKey(Procedencia, verbose_name='Procedencia')
 victima = models.ForeignKey(Victima, verbose_name='Victima')
 Agresor = models.ForeignKey(Agresor, verbose_name='Agresor')
 derecho = models.ForeignKey(Derecho, verbose_name='Derecho')
 status = models.BooleanField('Estado del la denuncia', default=False,
                              help_text='Indique el estado de la denuncia')
 carpeta = models.CharField('No. De Carpeta Judicial:', max_length=15,
                           help_text='Indique el número de carpeta judicial')
 description = models.TextField('Descripción', max_length=3500,
                           help_text='Descripción de la denuncia')
 created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Creado')
 modified = models.DateTimeField(auto_now=True, editable=False, verbose_name='Modificado')
 correlativo = models.IntegerField(editable=False, blank=True, null=True)

 def save(self, *args, **kwargs):  # redefinicion del metodo save() que contiene nuestro trigger

     if Denuncia.objects.all().count() == 0:
         codigo = Correlativo.objects.get().correlative
         if self.correlativo is None:
             res1 = 0
         else:
             res1 = codigo
         self.correlativo = res1 + codigo
     else:
         cor = Denuncia.objects.all()[Denuncia.objects.count()-1].correlativo
         self.correlativo = cor + 1
     # Aqui ponemos el codigo del trigger -------
     # fin de trigger ------
     return super(Denuncia, self).save(*args,
                                       **kwargs)  # llamada al save() original con sus parámetros correspondientes


 def __unicode__(self):
  return '%s' % self.correlativo


 class META:
    verbose_name = 'Denuncia'
    verbose_plural_name = 'Denuncias'

