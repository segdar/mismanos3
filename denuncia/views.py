# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

from denuncia.forms import DenunciaForm
from denuncia.models import Denuncia
from victima.models import Victima
from victima.forms import VictimaForm


# Create your views here
class DenunciaList(ListView):
    model = Denuncia
    template_name = 'crud/denuncia/denuncia_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(DenunciaList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(correlativo__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(procedencia__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(victima__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('denuncia.add_denuncia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DenunciaList, self).dispatch(*args, **kwargs)



class DenunciaCreate(CreateView):
    model = Denuncia
    form_class = DenunciaForm
    template_name = 'crud/denuncia/denuncia_form.html'
    success_url = reverse_lazy('denuncia:denuncia_listar')


    @method_decorator(
        permission_required('denuncia.add_denuncia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DenunciaCreate, self).dispatch(*args, **kwargs)


class DenunciaUpdate(UpdateView):
    model = Denuncia
    form_class = DenunciaForm
    template_name = 'crud/denuncia/denuncia_form.html'
    success_url = reverse_lazy('denuncia:denuncia_listar')


class DenunciaDelete(DeleteView):
    model = Denuncia
    template_name = 'crud/denuncia/denuncia_eliminar.html'
    success_url = reverse_lazy('denuncia:denuncia_listar')

