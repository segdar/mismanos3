from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from denuncia.views import DenunciaCreate, DenunciaList, \
    DenunciaDelete, DenunciaUpdate

# from django.conf import settings
# from django.conf.urls.static import static

# admin.autodiscover()

urlpatterns = [
                  url(r'^nuevo$', login_required(DenunciaCreate.as_view()), name='denuncia_crear'),
                  url(r'^listar/$', login_required(DenunciaList.as_view()), name='denuncia_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(DenunciaUpdate.as_view()), name='denuncia_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(DenunciaDelete.as_view()), name='denuncia_eliminar'),
              ]
