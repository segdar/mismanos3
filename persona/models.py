# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

SELECCION_GENERO = (

    ('M', 'Masculino'),
    ('F', 'Femenino'),
)
# Create your models here.


class Persona (models.Model):

 nombres = models.CharField('Nombres:', max_length=50, help_text='Ingrese los nombres separados por un espacio')
 apellidos = models.CharField('Apellidos:', max_length=50, help_text='Ingrese los apellidos separados por un espacio')
 edad = models.CharField('Edad:', max_length=3, help_text='Ingrese la Edad', blank=False)
 direccion= models.CharField('Dirección:', max_length=100, help_text='Ingrese Dirección')
 fechanacimiento= models.DateField('Fecha de Nacimiento', help_text='Ingrese Fecha de Nacimiento')
 cui = models.BigIntegerField('DPI/CUI:', help_text='Ingrese Numero De Identificación Personal', blank=True, null=True)
 gender = models.CharField('Genero:', max_length=1, choices=SELECCION_GENERO, default='M', help_text='Selección de Género')
 telefono = models.BigIntegerField('Telefono de Casa:', help_text = 'Ingrese un número de telefono de casa')
 created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Creado')
 modified = models.DateTimeField(auto_now=True, editable=False, verbose_name='Modificado')


 def __unicode__(self):
  return '%s %s' % (self.nombres, self.apellidos)


 class META:
    abstract = True