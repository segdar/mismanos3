# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-18 13:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tipo_diligencia', '0002_auto_20180518_1329'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tipo_Diligencia',
            new_name='Diligencia',
        ),
    ]
