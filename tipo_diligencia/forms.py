# -*- coding: utf-8 -*-
from django import forms
from tipo_diligencia.models import Diligencia

class DiligenciaForm(forms.ModelForm):
    class Meta:
        model = Diligencia

        fields = [
            'nombre',
        ]

        labels = {
            'nombre': 'Tipo:',

        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }


class Denuncia_DiligenciaForm(forms.ModelForm):
    class Meta:
        model =Diligencia

        fields = [
            'nombre',
        ]

        labels = {
            'nombre': 'Tipo:',
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }
