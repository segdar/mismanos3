from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from tipo_diligencia.views import DiligenciaList, DiligenciaCreate, \
    DiligenciaUpdate, DiligenciaDelete, DenunciaDiligenciaCreate

# from django.conf import settings
# from django.conf.urls.static import static

# admin.autodiscover()

urlpatterns = [
                  url(r'^nuevo$', login_required(DiligenciaCreate.as_view()), name='diligencia_crear'),
                  url(r'^listar/$', login_required(DiligenciaList.as_view()), name='diligencia_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(DiligenciaUpdate.as_view()), name='diligencia_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(DiligenciaDelete.as_view()), name='diligencia_eliminar'),
                  url(r'^nuevo_para_denuncia/$', login_required(DenunciaDiligenciaCreate.as_view()), name='dediladd'),
              ]
