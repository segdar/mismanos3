# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.template.context_processors import csrf
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator


from tipo_diligencia.forms import DiligenciaForm, Denuncia_DiligenciaForm
from tipo_diligencia.models import Diligencia


# Create your views here.

class DiligenciaList(ListView):
    model = Diligencia
    template_name = 'crud/Diligencia/diligencia_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(DiligenciaList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(nombre__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('tipo_diligencia.add_tipo_diligencia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DiligenciaList, self).dispatch(*args, **kwargs)

class DiligenciaCreate(CreateView):
    model = Diligencia
    form_class = DiligenciaForm
    template_name = 'crud/Diligencia/diligencia_form.html'
    success_url = reverse_lazy('diligencia:diligencia_listar')


    @method_decorator(
        permission_required('tipo_diligencia.add_tipo_diligencia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(DiligenciaCreate, self).dispatch(*args, **kwargs)

class DenunciaDiligenciaCreate(CreateView):
    model = Diligencia
    form_class = Denuncia_DiligenciaForm
    template_name = 'crud/Diligencia/diligencia_form_denuncia.html'
    success_url = reverse_lazy('denuncia:denuncia_crear')

"""
def add_alumno_encargado(request):
    if request.method == 'POST':
        form = Alumno_EncargadoForm(request.POST)
        if form.is_valid():
            new_persona = form.save()

            return HttpResponseRedirect(reverse('victima:alumno_crear'))
    else:
        form = Alumno_EncargadoForm()

    return render(request, 'encargado/encargado_form.html', {'form': form})
"""


class DiligenciaUpdate(UpdateView):
    model = Diligencia
    form_class = DiligenciaForm
    template_name = 'crud/Diligencia/diligencia_form.html'
    success_url = reverse_lazy('diligencia:diligencia_listar')


class DiligenciaDelete(DeleteView):
    model = Diligencia
    template_name = 'crud/Diligencia/diligencia_eliminar.html'
    success_url = reverse_lazy('diligencia:diligencia_listar')
