# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Procedencia (models.Model):

 nombre = models.CharField('Nombres:', max_length=50, help_text='Ingrese el nombre de la Procedencia')
 created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Creado')
 modified = models.DateTimeField(auto_now=True, editable=False, verbose_name='Modificado')


 def __unicode__(self):
  return '%s' % (self.nombre)


 class META:

     verbose_name = 'Procedencia'
     verbose_name_plural = 'Procedencias'
