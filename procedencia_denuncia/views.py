# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator


from procedencia_denuncia.forms import ProcedenciaForm, Procedencia_DiligenciaForm
from procedencia_denuncia.models import Procedencia


# Create your views here.

class ProcedenciaList(ListView):
    model = Procedencia
    template_name = 'crud/Procedencia/procedencia_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(ProcedenciaList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(nombre__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('procedencia_denuncia.add_procedencia_denuncia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(ProcedenciaList, self).dispatch(*args, **kwargs)


class ProcedenciaCreate(CreateView):
    model = Procedencia
    form_class = ProcedenciaForm
    template_name = 'crud/Procedencia/procedencia_form.html'
    success_url = reverse_lazy('procedencia:procedencia_listar')


    @method_decorator(
        permission_required('procedencia_denuncia.add_procedencia_denuncia', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(ProcedenciaCreate, self).dispatch(*args, **kwargs)


class ProcedenciaDenunciaCreate(CreateView):
    model = Procedencia
    form_class = Procedencia_DiligenciaForm
    template_name = 'crud/Procedencia/procedencia_form_denuncia.html'
    success_url = reverse_lazy('denuncia:denuncia_crear')

"""
by: Eddav3 follow me on twitter
"""


class ProcedenciaUpdate(UpdateView):
    model = Procedencia
    form_class = ProcedenciaForm
    template_name = 'crud/Procedencia/procedencia_form.html'
    success_url = reverse_lazy('procedencia:procedencia_listar')


class ProcedenciaDelete(DeleteView):
    model = Procedencia
    template_name = 'crud/Procedencia/procedencia_eliminar.html'
    success_url = reverse_lazy('procedencia:procedencia_listar')
