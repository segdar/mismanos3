from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from procedencia_denuncia.views import ProcedenciaList, ProcedenciaCreate, \
    ProcedenciaUpdate, ProcedenciaDelete, ProcedenciaDenunciaCreate

# from django.conf import settings
# from django.conf.urls.static import static

# admin.autodiscover()

urlpatterns = [
                  url(r'^nuevo$', login_required(ProcedenciaCreate.as_view()), name='procedencia_crear'),
                  url(r'^listar/$', login_required(ProcedenciaList.as_view()), name='procedencia_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(ProcedenciaUpdate.as_view()), name='procedencia_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(ProcedenciaDelete.as_view()), name='procedencia_eliminar'),
                  url(r'^nuevo_para_denuncia/$', login_required(ProcedenciaDenunciaCreate.as_view()), name='dediladd'),
              ]
