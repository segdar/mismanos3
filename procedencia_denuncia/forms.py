# -*- coding: utf-8 -*-
from django import forms
from procedencia_denuncia.models import Procedencia

class ProcedenciaForm(forms.ModelForm):
    class Meta:
        model = Procedencia

        fields = [
            'nombre',
        ]

        labels = {
            'nombre': 'Procedencia:',

        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }


class Procedencia_DiligenciaForm(forms.ModelForm):
    class Meta:
        model =Procedencia

        fields = [
            'nombre',
        ]

        labels = {
            'nombre': 'Procedencia:',
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }
