from django.contrib.auth.decorators import login_required
from django.conf.urls import url
from agresor.views import AgresorList, \
    AgresorUpdate, AgresorDelete, AgresorCrear


urlpatterns = [
                  url(r'^nuevo', login_required(AgresorCrear.as_view()), name='agresor_crear'),
                  url(r'^listar', login_required(AgresorList.as_view()), name='agresor_listar'),
                  url(r'^editar/(?P<pk>\d+)/$', login_required(AgresorUpdate.as_view()), name='agresor_editar'),
                  url(r'^eliminar/(?P<pk>\d+)/$', login_required(AgresorDelete.as_view()), name='agresor_eliminar'),
              ]
