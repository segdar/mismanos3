# -*- coding: utf-8 -*-
from django import forms
from agresor.models import Agresor

class DateInput(forms.DateInput):
    input_type = 'date'
    format = ('d-m-Y')

class AgresorForm(forms.ModelForm):
    class Meta:
        model = Agresor
        fields = [
            'nombres',
            'apellidos',
            'edad',
            'direccion',
            'fechanacimiento',
            'cui',
            'gender',
            'telefono',
            'fotografia',
        ]

        labels = {
            'nombres': 'Nombres:',
            'apellidos': 'Apellidos:',
            'edad': 'Edad:',
            'direccion': 'Dirección:',
            'fechanacimiento': 'Fecha Nacimiento:',
            'cui': 'CUI/DPI:',
            'gender': 'Genero:',
            'telefono': 'Teléfono:',
        }

        widgets = {
            'nombres': forms.TextInput(attrs={'class': 'form-control'}),
            'apellidos': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.TextInput(attrs={'class': 'form-control'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control'}),
            'fechanacimiento':   DateInput(attrs={'class': 'form-control'}),
            'cui': forms.TextInput(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control'}),
        }
