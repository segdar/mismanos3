# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q
import operator
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

from agresor.forms import AgresorForm
from agresor.models import Agresor


# Create your views here
class AgresorList(ListView):
    model = Agresor
    template_name = 'crud/agresor/agresor_listar.html'
    paginate_by = 10
    ordering = ["-id"]

    def get_queryset(self):
        result = super(AgresorList, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(nombres__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(apellidos__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(cui__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(telefono__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(direccion__icontains=q) for q in query_list))
            )

        return result

    @method_decorator(
        permission_required('agresor.add_agresor', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(AgresorList, self).dispatch(*args, **kwargs)


class AgresorCrear(CreateView):
    model = Agresor
    form_class = AgresorForm
    template_name = 'crud/agresor/agresor_form.html'
    success_url = reverse_lazy('agresor:agresor_listar')

    @method_decorator(
        permission_required('agresor.add_agresor', reverse_lazy('dashboard:dashboard_inicio')))
    def dispatch(self, *args, **kwargs):
        return super(AgresorCrear, self).dispatch(*args, **kwargs)


class AgresorUpdate(UpdateView):
    model = Agresor
    form_class = AgresorForm
    template_name = 'crud/agresor/agresor_form.html'
    success_url = reverse_lazy('agresor:agresor_listar')


class AgresorDelete(DeleteView):
    model = Agresor
    template_name = 'crud/agresor/agresor_eliminar.html'
    success_url = reverse_lazy('agresor:agresor_listar')
